import os
import pandas as pd
import numpy as np
import seaborn as sb
from matplotlib import pyplot as plt


# понижение регистра категориальных данных
def lower(data):

    for col in data.columns:

        if data[col].dtype == "object":

            data[col] = data[col].str.lower()


# заполнение пропусков в столбце 2
# на основании наиболее часто встречающихся значений для каждого значения столбца 2
def fill_null(data, column1, column2):

    grouped_data = data.groupby(column1)

    for name, group in grouped_data:

        if not group[column2].isnull().all():

            most_common_value = group[column2].mode().iloc[0]

            data.loc[(data[column1] == name) & (data[column2].isnull()),
                     column2] = most_common_value


# Заполнение пропусков
def fill_passes(data):

    for col in data.columns:

        if data[col].dtype == "object":

            data[col] = data[col].fillna('unknown')

        if (data[col].dtype == "float") or (data[col].dtype == "int"):

            data[col] = data[col].fillna(-1)


# Так как вместо названия района попадаются объясления и ненужный текст,
# создаём правило, которое оставит только тест, содержадий не более 3х слов
def remove_long_text(text):

    words = text.split()

    if len(words) > 3:

        return None

    else:

        return text


# создание таблицы
def get_analysis_data() :

    if os.path.exists("../../data/raw/data.csv"):

        df_data = pd.read_csv('../../data/raw/data.csv')

        lower(df_data)

        fill_null(df_data, 'underground', 'district')
        fill_null(df_data, 'street', 'district')
        fill_null(df_data, 'district', 'underground')

        fill_passes(df_data)

        df_data = df_data.drop_duplicates()

        df_data['price_per_m2'] = df_data['price_per_m2'].abs()

        df_data['clean_district'] = df_data['district'].apply(remove_long_text)
        df_data.dropna(subset=['clean_district'], inplace=True)
        df_data.drop('district', axis=1, inplace=True)
        df_data = df_data.rename(columns={'clean_district' : 'district'})

        df_data = df_data.drop(['deal_type', 'accommodation_type', 'city', 'phone'], axis=1)


        try:
            os.ulink("../../data/interim/data.csv")
        except:
            print("Error while deleting file")

        df_data.to_csv(r'../../data/interim/data.csv', index=False)


        return 0

    else:

        return 1


# создание таблицы
def get_prediction_data() :

    if os.path.exists("../../data/interim/data.csv"):

        df_data = pd.read_csv('../../data/interim/data.csv')

        df_data = df_data.drop(['street', 'author'], axis=1)

        df_data = df_data.query('rooms_count>0')

        # Ограничу площадь 9 кв. и 80% квартилем
        q90=data1['total_meters'].quantile(0.9)
        data1 = data1.query('total_meters >9 and total_meters <= @q90')


        #определение 10% и 90% квартилей, чтобы всё, что выходит за рамки убрать
        q10 = data1['price_per_m2'].quantile(0.1)
        q80 = data1['price_per_m2'].quantile(0.8)
        data1 = data1.query('price_per_m2 >= @q10 and price_per_m2 <= @q80')

        # убираю цены, которые встречаются крайне редко
        data1=data1.query('1000000<price<100000000')


        #Для обучения модели оставляем датасет без ссылок, номера телефона, жилой площади и площади кухни (данных по ним практически нет)
        data_model=data1.drop(['link'], axis=1)
        data_model.to_csv('data_model.csv',index=False)

        #Сохраняем таблицу с ссылками
        data1=data1.drop(['living_meters', 'kitchen_meters', 'phone'], axis=1)
        data1.to_csv('data_fin.csv',index=False)


        return 0

    else :

        return 1
